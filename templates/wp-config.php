<?php
define( 'DB_NAME', '{{ DB_NAME }}' );
define( 'DB_USER', '{{ DB_USER }}' );
define( 'DB_PASSWORD', '{{ DB_PW }}' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

define( 'AUTH_KEY',         '}2%Wq>7E+En7ri(9_:N`/!dj}].*YFzZo0Mx>1AVEOuCgq><-a;iWM_H3OjrTCBl' );
define( 'SECURE_AUTH_KEY',  '/(g|/R${&1GJ2LNR28[#,3g7^iiIw&0ShCw.[g(esKw:9IZ<@pL3`4PCuWu /tw>' );
define( 'LOGGED_IN_KEY',    ']04l@cJbQR3MT<;T2bzhPr8EXNhx0*Ni.GN;3]/#!p>EQ4?_Ww1wh7@MjJe AI4H' );
define( 'NONCE_KEY',        'T2hD_d>[lJ9x]sj.R[>TtNkUeaf8x3u2$}l+EfKbQEEhtZC@!dF7)am?|#XFpK#~' );
define( 'AUTH_SALT',        '[t`83i|6cG:+=+A:wt.JDaB!G[$_LDjm2-r_%_F/ua5<KPWbvs>t  `t]MB-M],z' );
define( 'SECURE_AUTH_SALT', 'OTHd!h,u3>~|i=}OCw1g^?YXEzKsa:X@(c9D*f&bd4]h_8VqWk=ClO=Xr=cAUF4K' );
define( 'LOGGED_IN_SALT',   '2Yv*^Xs{rf`~Dtl4wQQ]?h91>$$EIvpe&R2YNY-uSlqS+AZhm=hXR?lGgw>ef9R[' );
define( 'NONCE_SALT',       '<g(Khg.kCDs#!5EClbP@ Cfpo0cU:P|XIsYEW=Ym44e49kT6`*X6<+}2K-=rYVg+' );

$table_prefix = 'wp_';

define( 'WP_DEBUG', false );

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
require_once( ABSPATH . 'wp-settings.php' );

